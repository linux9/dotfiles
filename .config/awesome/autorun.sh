#!/bin/bash

function run {
  if ! pgrep -f $1 ;
	    then
	        $@&
  fi
}

run setxkbmap -layout gb
run nitrogen --restore
run volumeicon
run udiskie
-- run xrandr --output DP-0 --mode 3440x1440 --rate 100
run polychromatic-tray-applet
-- run ulauncher
run picom -b --config $HOME/.config/picom/picom.conf
-- run nm-applet
-- run pamac-tray
-- run flameshot
-- run xrandr --output DP-4 --mode 3440x1440 --rate 100
run xautolock -detectsleep -time 3 -locker \"i3lock -c 000000\"
