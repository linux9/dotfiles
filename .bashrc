
# Note: PS1 and umask are already set in /etc/profile. You should not
# need this unless you want different defaults for root.
# PS1='${debian_chroot:+($debian_chroot)}\h:\w\$ '
# umask 022

# You may uncomment the following lines if you want `ls' to be colorized:
#export LS_OPTIONS='--color=auto'
#eval "$(dircolors)"
#alias ls='ls $LS_OPTIONS'
#alias ll='ls $LS_OPTIONS -l'
#alias l='ls $LS_OPTIONS -lA'
#
# Some more alias to avoid making mistakes:
# alias rm='rm -i'
# alias cp='cp -i'
# alias mv='mv -i'

# Source global definitions
if [ -f /etc/bashrc ]; then
	 . /etc/bashrc
fi

[[ $- == *i* ]] && source ~/.local/share/blesh/ble.sh --noattach

# Enable bash programmable completion features in interactive shells
# Use bash-completion, if available
[[ $PS1 && -f /usr/share/bash-completion/bash_completion ]] && \
    . /usr/share/bash-completion/bash_completion

export HISTORY_IGNORE="(ls|cd|pwd|exit|sudo reboot|history|cd -|cd ..)"
# Don't put duplicate lines in the history and do not add lines that start with a space
export HISTCONTROL=erasedups:ignoredups:ignorespace
PROMPT_COMMAND='history -a'

# Install Ruby Gems to ~/gems
export GEM_HOME="$(ruby -e 'puts Gem.user_dir')"
export PATH="$PATH:$GEM_HOME/bin"
#export QT_QPA_PLATFORMTHEME=gnome

export PAGER="most"

export PATH=$PATH:/usr/local/go/bin
export PATH=$PATH:$HOME/.bin
export PATH=$PATH:$HOME/.dotnet
export PATH=$PATH:$HOME/.dotnet/shared
export PATH=$PATH:$HOME/.dotnet/tools
export PATH=$PATH:$HOME/.config/rofi/scripts

unset GDK_BACKEND

if [[ -x "$(command -v dotnet)" ]]; then
  export DOTNET_ROOT="$(dirname $(which dotnet))"
fi

export EDITOR="vim"

if [[ $- == *i* ]]; then # in interactive session
  set -o vi
fi

eval "$(starship init bash)"

[ -f ~/.fzf.bash ] && source ~/.fzf.bash

# CD COMM

bettercd() {
  cd $1
  if [ -z $1 ]
    then
      selection="$(ls -a | fzf --height 40% --reverse)"
      if [[ -d "$selection" ]]
      then
        cd "$selection"
      elif [[ -f "$selection" ]]
      then
        $EDITOR "$selection"
      fi
  fi
}

fuzzydir() {
  selection="$(find * -type d | fzf --height 40% --reverse)"
  if [[ -z $selection ]]
  then
    return
  fi
  cd "$selection"
}

fuzzyfile() {
  selection="$(find * -type f | fzf --height 40% --reverse)"
  if [[ -z $selection ]]
  then
    return
  fi
  $EDITOR "$selection"
}

export GREP_OPTIONS='--color=always'
export EZA_OPTIONS='--icons=auto --group-directories-first --sort=name --long --git'

alias grep='grep $GREP_OPTIONS'
alias eza='eza $EZA_OPTIONS'
alias ls='eza'
alias tree='eza --tree'
alias h='history'
alias ll='ls -l'
alias z='fuzzydir'
alias f='fuzzyfile'
alias aws='docker run --rm -it -v ~/.aws:/root/.aws amazon/aws-cli'
alias config='/usr/bin/git --git-dir=$HOME/.cfg/ --work-tree=$HOME'
alias cat='bat'

# nvm
export NVM_DIR="$([ -z "${XDG_CONFIG_HOME-}" ] && printf %s "${HOME}/.nvm" || printf %s "${XDG_CONFIG_HOME}/nvm")"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh" # This loads nvm

load-nvmrc() {
  local node_version="$(nvm version)"
  local nvmrc_path="$(nvm_find_nvmrc)"

  if [ -n "$nvmrc_path" ]; then
    local nvmrc_node_version=$(nvm version "$(cat "${nvmrc_path}")")

    if [ "$nvmrc_node_version" = "N/A" ]; then
      nvm install
    elif [ "$nvmrc_node_version" != "$node_version" ]; then
      nvm use
    fi
  elif [ "$node_version" != "$(nvm version default)" ]; then
    echo "Reverting to nvm default version"
    nvm use default
  fi
}

load-nvmrc

# autojump
[[ -s /etc/profile.d/autojump.sh ]] && source /etc/profile.d/autojump.sh
[[ -s /usr/share/autojump/autojump.sh ]] && source /usr/share/autojump/autojump.sh

# Terminal start up
if [[ -x "$(command -v misfortune)" ]] \
  && [[ -x "$(command -v cowsay)" ]] \
  && [[ -x "$(command -v figlet)" ]] \
  && [[ -x "$(command -v lolcat)" ]]; then

  hostname | figlet -f 3d | lolcat
  misfortune -o | cowsay -f tux | lolcat
fi
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

#source ~/.local/share/blesh/ble.sh
[[ ! ${BLE_VERSION-} ]] || ble-attach
